package org.javasoft.insuredbackend.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class MessageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long messageId;

    @Lob
    @Column(columnDefinition = "TEXT")
    private String text;
}
