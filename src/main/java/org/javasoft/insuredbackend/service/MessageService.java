package org.javasoft.insuredbackend.service;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.insuredbackend.entity.MessageEntity;
import org.javasoft.insuredbackend.model.Message;
import org.javasoft.insuredbackend.model.Response;
import org.javasoft.insuredbackend.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

@Slf4j
@Service
public class MessageService {

    private MessageRepository messageRepository;

    public Response saveMessage(Message message){
        final val messageEntity = new MessageEntity();
        messageEntity.setText(message.getMessage());
        messageRepository.save(messageEntity);
        return Response.builder()
                .response("[ " + HtmlUtils.htmlEscape(message.getMessage()) + " ] ")
                .build();
    }

    @Autowired
    public void setMessageRepository(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }
}
