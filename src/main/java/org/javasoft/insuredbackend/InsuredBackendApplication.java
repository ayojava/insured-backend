package org.javasoft.insuredbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuredBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsuredBackendApplication.class, args);
    }

}
