package org.javasoft.insuredbackend.model;

import lombok.Data;

@Data
public class Message {

    private String from;

    private String message;
}
